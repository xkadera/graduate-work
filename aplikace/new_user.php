<?php
session_start();

if (isset($_SESSION['us_name']) AND isset($_SESSION['us_password'])AND $_SESSION['admin']==1){
	require_once("db_connect.php");
	require_once("kostra.php");
	head();
?>

<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
         

          <!-- Content Row -->
          <div class="row">
          </div>


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Nový uživatel</h6>
            </div>
            <div class="card-body">
              
 			<?php 
              	if(isset($_SESSION['error'])){
              		echo $_SESSION['error'];
              		unset($_SESSION['error']);
              	}
              ?>

					<form class="user" action="new_user_zpracuj.php" method="POST">

					 <div class="form-group">
                    	
                      <input type="text" class="form-control form-control-user" placeholder="Uživatelské jméno" name="us_name">

                    </div>

                    <div class="form-group">

                      <input type="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="E-mail" name="us_mail">

                    </div>

                    <div class="form-group">
                    	
                      <input type="password" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Heslo" name="us_password">

                    </div>

                    <div class="form-group">
                    	
                      <input type="password" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Heslo" name="us_password2">

                    </div>

                     <div class="form-group">
                    	
                      <input type="checkbox"  id="admin" aria-describedby="emailHelp"  name="admin" value="1">
                      <label for="admin">Administrátor</label>
                    </div>

                    <div class="form-group">
                    	
                      <input type="checkbox" id="mail" aria-describedby="emailHelp"  name="mail" value="1">
                      <label for="mail">Posílat E-maily</label>
                    </div>

                    
                    
                    <input type="submit" value="Vytvořit" name="submit" class="btn btn-primary btn-user btn-block">
                  </form>



            </div>
          </div>



        </div>

<?php
	pata();
}else{
  header ("Location: user.php"); 
}


?>