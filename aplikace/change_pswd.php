<?php
session_start();
if (isset($_SESSION['us_name']) AND isset($_SESSION['us_password'])){
require_once("db_connect.php");
require_once("kostra.php");
head();

?>

 <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
         

          <!-- Content Row -->
          <div class="row">
          </div>


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Změna hesla</h6>
            </div>
            <div class="card-body">
              <?php 
              	if(isset($_SESSION['error'])){
              		echo $_SESSION['error'];
              		unset($_SESSION['error']);
              	}
              ?>
					<form class="user" action="change_pswd_zpracuj.php" method="POST">
					
                    <div class="form-group">
                    	
                      <input type="password" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Staré heslo" name="old_password">

                    </div>

                    <div class="form-group">

                      <input type="password" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Nové heslo" name="new_password">

                    </div>

                    <div class="form-group">

                      <input type="password" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Nové heslo" name="new_password2">

                    </div>
                    
                    
                    <input type="submit" value="Změnit" name="submit" class="btn btn-primary btn-user btn-block">
                  </form>



            </div>
          </div>



        </div>



<?php
pata();
}else{
  header ("Location: index.php"); 
}
?>