<?php
session_start();
if (isset($_SESSION['us_name']) AND isset($_SESSION['us_password'])){
require_once("db_connect.php");
require_once("kostra.php");
head();
?>



       

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
         

          <!-- Content Row -->
          <div class="row">
          </div>


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Přehled</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              

              <?php
                if(isset($_SESSION['error'])){
                  echo $_SESSION['error'];
                  unset($_SESSION['error']);
                }
                ?>

                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                      <tr>
                      <td>Čas</td>
                      <td>Místo</td>
                       <td>Typ senzoru</td>
                      <td>Foto</td>
                      </tr>
                  </thead>
                  <tfoot>
                    <tr>
                    <td>Čas</td>
                    <td>Místo</td>
                    <td>Typ senzoru</td>                   
                    <td>Foto</td>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php
                        $sql = "SELECT * FROM sensor ORDER BY id DESC";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
       
   
                                while($row = $result->fetch_assoc()) {
                                    echo "<tr><td>" . 
                                    $row["time"] . "</td><td>" .
                                    $row["location"] . "</td> <td> " . 
                                    $row["type"] . "</td>
                                    <td><a class='ikona-link' href='" .  $row["url"] . "' data-lightbox='roadtrip'><img class='ikona' src='" .  $row["url"] . "' ></a></td>";

                                    ?>
                                    <td><a href="delete.php?url=<?php echo $row["url"];?>&id=<?php echo $row["id"]; ?>" onclick="return confirm('Odstranit záznam?')">Smazat</a></td></tr>
                                    <?php

                                  
                                }
                                
                            } else {
                                echo "0 results";
                            }
                            $conn->close();
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>



        </div>
        <!-- /.container-fluid -->
<?php
pata();

}else{
  header ("Location: index.php"); 
}
?>

      
