<?php
session_start();
if (isset($_SESSION['us_name']) AND isset($_SESSION['us_password']) AND $_SESSION['admin']==1){
	require_once("db_connect.php");
	require_once("kostra.php");
	head();
?>

  <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
         

          <!-- Content Row -->
          <div class="row">
          </div>


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Všichni uživatelé</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              	<?php 
              	if(isset($_SESSION['error'])){
              		echo $_SESSION['error'];
              		unset($_SESSION['error']);
              	}
              ?>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                      <tr>
                      <td>Jméno</td>
                      <td>E-mail</td>
                       <td>Oprávění</td>
                      <td>Zasílat E-maily</td>
                      </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <td>Jméno</td>
                      <td>E-mail</td>
                      <td>Oprávění</td>
                      <td>Zasílat E-maily</td>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php
                        $sql = "SELECT * FROM users ORDER BY id ASC";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
       
   
                                while($row = $result->fetch_assoc()) {
                                	if($row["admin"]==1){
                                		$admin="administrátor";

                                	}else{
                                		$admin="uživatel";
                                	}

                                	if($row["send_mail"]==1){
                                		$send_mail="Ano";
                                	}else{
                                		$send_mail="Ne";
                                
                                	}

                                    echo "<tr><td>" . 
                                    $row["us_name"] . "</td><td>" .
                                    $row["us_mail"] . "</td><td>" . $admin . "<br>";?>
                                    <a href="change_user.php?id=<?php echo $row["id"];?>&change=admin&from=<?php echo $row["admin"]?>" onclick="return confirm('Změnit oprávnění?')">Změnit</a></td>

                                    <td>
                                    <?php echo $send_mail . "<br>";?>
                                    <a href="change_user.php?id=<?php echo $row["id"];?>&change=mail&from=<?php echo $row["send_mail"]?>" onclick="return confirm('Změnit zasílaní Emailů?')">Změnit</a></td>
                                    </td>
                                    <td><a href="delete_user.php?id=<?php echo $row["id"];?>" onclick="return confirm('Odstranit uživatele?')">Smazat</a></td></tr>
                                    <?php

                                  
                                }
                                
                            } else {
                                echo "0 results";
                            }
                            $conn->close();
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>



        </div>












<?php
	pata();
}else{
  header ("Location: user.php"); 
}


?>